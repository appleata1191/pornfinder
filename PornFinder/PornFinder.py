#!/usr/bin/env python3

import random
import time

import requests
from bs4 import BeautifulSoup
from requests_html import HTMLSession, HTML


class Parser(object):
    def __init__(self):
        self.ua = open('ua-list', 'r').readlines() 
        self.websites = [
                {'website':'http://www.258porn.com',
                 'data':'article',
                 'title':'h2 a'},
                {'website':'http://www.7bam5.xyz/recent/',
                 'data':'.videos li',
                 'title':'.video-title'},
                {'website':'https://52jav.men/',
                 'data':'.item'},
                {'website':'http://javmovie.com/zh',
                 'data':'.movie-item',
                 'title':'.movie-title'},
                {'website':'https://spankbang.com/',
                 'data':'.video-item',
                 'title':'.inf',
                 'img':'.thumb img',
                 'img-src':'data-src'},
                {'website':'https://cn.pornhub.com/video?o=cm',
                 'data':'.pcVideoListItem',
                 'title':'.title',
                 'img-src':'data-src'},
                {'website':'https://xhamster.com/',
                 'data':'.thumb-list__item',
                 'url':'.video-thumb-info a',
                 'title':'.video-thumb-info a'}, 
                {'website':'https://www.youporn.com/browse/time/',
                 'data':'.video-box',
                 'img':'.video-box-image-wrapper img',
                 'img-src':'data-original'},
                {'website':'https://www.redtube.com/newest',
                 'data':'.videoblock_list',
                 'title':'.video_title',
                 'img-src':'data-src'},
                {'website':'https://hpjav.tv/',
                 'data':'.col-sm-6',
                 'title':'.entry-title a',
                 'img-src':'data-original'},
                {'website':'https://yase.app/',
                 'data':'.video-item',
                 'img':'.video-thumb-image',
                 'img-src':'data-original'},
                {'website':'https://fap18.net/latest/all',
                 'data':'#content li',
                 'title':'.title'},
                {'website':'https://69vj.com/',
                 'data':'.video',
                 'title':'.title'},
                {'website':'https://www.qzav21.com/latest-updates/',
                 'data':'.item',
                 'img-src':'data-original'},
                {'website':'https://seseda95.com/',
                 'data':'.video-card',
                 'title':'.video-title'},
                {'website':'https://85tube.com/',
                 'data':'.item',
                 'img-src':'data-original'},
                {'website':'https://av6k.com/index.html',
                 'data':'.listA'},
                {'website':'https://porn87.com/',
                 'data':'.column-block'},
                {'website':'https://see.xxx/all/latest/',
                 'data':'.thum1',
                 'title':'.info'},
                {'website':'https://www.javdove.com/',
                 'data':'.col-video'},
                {'website':'https://www.xvideos.com/new/1/',
                 'data':'.thumb-block',
                 'title':'.title'},
                 
                 ]

    def pickUA(self):
        return random.choice(self.ua).strip()

    def get_page(self, url):
        s = HTMLSession()
        try:
            headers = {'user-agent':self.pickUA()}
            rsp = s.get(url, headers=headers, timeout=30)
            rsp.html.render()
            return rsp.html.html
        except Exception as e:
            print(f'Error {e}')
        return ''


    def getDatasFromWebsite(self, website):
        datas = []
        
        website['title'] = website.get('title', 'a')
        website['url'] = website.get('url', 'a')
        website['img'] = website.get('img', 'img')
        website['img-src'] = website.get('img-src', 'src')

        rsp = self.get_page(website['website'])

        soup = BeautifulSoup(rsp, 'html.parser')
        for each in soup.select(website['data']):
            data = {}
            data['title'] = each.select(website['title'])[0].text.strip()
            data['url'] = each.select(website['url'])[0].get('href')
            data['img'] = each.select(website['img'])[0].get(website['img-src'])
            for tag in ['url', 'img']:
                if data[tag].startswith('//'):
                    data[tag] = 'http:' + data[tag]
                if not data[tag].startswith('http'):
                    url = website['website'].split(':')[0] + '://' + website['website'].split('://')[1].split('/')[0]
                    data[tag] = url + data[tag]


            datas.append(data)

        return datas

    def getDatas(self):
        datas = []
        for website in self.websites:
            try:
                new_datas = self.getDatasFromWebsite(website)
                if len(new_datas) == 0:
                    print(f'{website["website"]} - {len(new_datas)}')
                datas += new_datas
            except Exception as exp:
                print(f'{website["website"]} - {str(exp)}')
                pass

        return datas

    
    def uploadToServer(self, datas):
        for data in datas:
            data['passwd'] = UPLOAD_PASSWORD
            try:
                res = requests.post(ARTICLE_PLUS, data=data, timeout=30).text
            except:
                pass

        


if __name__ == '__main__':
    try:
        bot = Parser()
        datas = bot.getDatas()
    except Exception as exp:
        err = open('error', 'a')
        err.write(str(exp))
        err.close()

    #for data in datas:
    #    print(data)
    #while 1:
    #    print('='*30)
    #    print(f'Start fetch {datetime.datetime.now()}')
    #    print('='*30)
    #    try:
    #        bot = NewsBot()
    #        articles = bot.getNews()
    #        bot.uploadToServer(articles)
    #    except Exception as exp:
    #        err = open('error', 'a')
    #        err.write(str(exp))
    #        err.close()
    #    time.sleep(15*60)






