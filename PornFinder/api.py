import requests

class Bot:
    def __init__(self, url, email, nickname, password):
        self.url = url
        self.data = {'email':email, 'nickname':nickname, 'password':password}
        self.headers = {}

    def register(self):
        rsp = requests.post(self.url + '/api/register', self.data)
        self.token = rsp.json()['token']

    def login(self):
        rsp = requests.post(self.url + '/api/login', self.data)
        self.headers = { 'Authorization':'Bearer ' + rsp.json()['token'] }



if __name__ == '__main__':
    url = 'http://127.0.0.1:5000'
    bot = Bot(url, 'aaa', 'aaa', 'aaaaaa')
    bot.login()

