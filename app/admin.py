from datetime import datetime, timedelta
from flask_admin.contrib.sqla import ModelView

class UserView(ModelView):
    column_labels = {
        'id':'id',
        'nickname':'使用者姓名',
        'email':'電子信箱',
        'role':'角色',
        'create_time':'建立時間'}

    column_list = ('id', 'nickname', 'email', 'role', 'create_time')
    column_searchable_list = ['nickname', 'email']
    column_filters = ['role']
    form_columns = ('nickname', 'email', 'role', 'password')

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.create_time = int(datetime.now().timestamp()) 

    def _create_time_formatter(view, context, model, name):
        if model.create_time:
            dt = datetime.utcfromtimestamp(model.create_time)
            dt += timedelta(hours=8)
            return dt.strftime('%Y-%m-%d %H:%M') 
        else:
           return ""

    column_formatters = {
        'create_time': _create_time_formatter
    }

    #def __init__(self, session, **kwargs):
    #    super(UserView, self).__init__(session, **kwargs)

class WebsiteView(ModelView):
    column_labels = {
        'id':'id',
        'name':'網站名',
        'rule':'抓取模版',
        'create_time':'建立時間'}

    column_list = ('id', 'name', 'rule', 'create_time')
    form_excluded_columns = ('id', 'create_time',)

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.create_time = datetime.now()
    #def __init__(self, session, **kwargs):
    #    super(UserView, self).__init__(session, **kwargs)

class VideoView(ModelView):
    column_labels = {
        'id':'id',
        'title':'標題',
        'original_url':'原始網址',
        'short_code':'短網址',
        'img':'圖片網址',
        'click':'點擊次數',
        'create_time':'建立時間'}

    column_list = ('id', 'title', 'original_url', 'short_code', 'img', 'click', 'create_time')
    form_excluded_columns = ('id', 'create_time',)

    def on_model_change(self, form, model, is_created):
        if is_created:
            model.create_time = datetime.now()
