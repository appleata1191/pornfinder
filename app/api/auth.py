from datetime import datetime, timedelta

import jwt
from flask import jsonify, request
from flask_login import login_user, login_required, logout_user, current_user
from flask_jwt_extended import create_access_token, jwt_required, jwt_optional, get_jwt_identity

from . import api
from app.models import db
from app.models.user import User
from app.forms.auth import LoginForm, RegisterForm

@api.route('/register', methods=['POST'])
def register():
    form = RegisterForm(request.form)
    if form.validate():
        email = form.email.data
        nickname = form.nickname.data
        password = form.password.data
        user = User(email=email, nickname=nickname, password=password)
        db.session.add(user)
        db.session.commit()
        token = create_access_token(identity={'uid':user.id, 'role':user.role})
        return token
    return jsonify(form.errors)

@api.route('/login', methods=['POST'])
def login():
    form = LoginForm(request.form)
    if form.validate():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(form.password.data):
            token = create_access_token(identity={'uid':user.id, 'role':user.role})
            return jsonify({'token':token})
        else:
            return jsonify(form.errors)
    return jsonify(form.errors)


@api.route('/aaa', methods=['GET'])
@jwt_optional
def aaa():
    identity = get_jwt_identity()
    if identity:
        return jsonify(identity)
    return 'no login'



