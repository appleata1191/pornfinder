from flask import Blueprint, url_for

api = Blueprint('api', __name__, template_folder='templates')

from app.api import video
from app.api import website
from app.api import auth
