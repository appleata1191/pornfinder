from datetime import datetime, timedelta
import json

from flask import jsonify, request

from . import api
from app.models import db
from app.models.video import Video

def _check_dup_url(urls):
    '''
        回傳沒有重複到的影片網址
    '''
    videos = Video.query.filter(Video.original_url.in_(urls)).all()
    exist_url = set([video.original_url for video in videos])
    return set(urls) - exist_url

@api.route('/videos', methods=['POST'])
def add_videos():
    '''
        新增影片
    '''
    datas = request.get_json()
    urls  = [data['url'] for data in datas ]
    urls  = _check_dup_url(urls)
    datas = [data for data in datas if data['url'] in urls]

    new_videos = []
    for data in datas:
        video = Video(data['title'], data['url'], data['short_code'], data['img'])
        new_videos.append(video)

    db.session.add_all(new_videos)
    db.session.commit()

    return jsonify({'code':'ok'})


@api.route('/videos/check', methods=['POST'])
def check_videos():
    '''
        檢查之前是否有重複的影片網址
    '''
    datas = request.get_json()
    urls = _check_dup_url(datas['urls'])
    return jsonify(list(urls))


    





