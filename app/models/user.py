from flask import current_app
from flask_login import UserMixin
from sqlalchemy import Column, String, Integer, Enum
from sqlalchemy import event
from werkzeug.security import generate_password_hash, check_password_hash

from app.models.base import db, Base
from app import login_manager

class User(UserMixin, Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    nickname = Column(String(24), nullable=False, unique=True)
    email = Column(String(50), unique=True, nullable=False)
    password = Column(String(100))
    role = Column(Enum('guest', 'member', 'admin'), server_default='guest', nullable=False)

    def __init__(self, nickname, email, password):
        self.nickname = nickname
        self.email = email 
        self.password = password
        self._password = password
        super(User, self).__init__()

    def reset_password(self, new_password):
        self.password = new_password

    def check_password(self, raw):
        if not self.password:
            return False
        return check_password_hash(self.password, raw)



@login_manager.user_loader
def get_user(uid):
    return User.query.get(int(uid))
    

@event.listens_for(User.password, 'set', retval=True)
def hash_user_password(target, value, oldvalue, initiator):
    if value != oldvalue:
        return generate_password_hash(value)
    return value
