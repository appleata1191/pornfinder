import json

from sqlalchemy import Column, String, Integer, Text

from app.models.base import Base

class Video(Base):
    '''
        紀錄所有的影片網址
    '''

    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(256), index=True)
    original_url = Column(String(512))
    short_code = Column(String(10))     # 縮網址的簡碼
    img = Column(String(512))           # 圖片網址
    click = Column(Integer, default=0)  # 點擊次數

    
    def __init__(self, title, original_url, short_code, img):
        self.title = title
        self.original_url = original_url
        self.short_code = short_code
        self.img = img
        super(Video, self).__init__()

    def __repr__(self):
        return f'<Vedio : {self.title}>'

    def __str__(self):
        return f'<Vedio : {self.title}>'

