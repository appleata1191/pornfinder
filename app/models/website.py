import json

from sqlalchemy import Column, String, Integer, Text

from app.models.base import Base

class Website(Base):
    '''
        紀錄有那些要爬的網站跟他的Rules
    '''

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50))
    rule = Column(Text)     # 抓取網站的Rule, 需要為Json格式的字串

    def __init__(self, name, rule):
        self.name = name
        self.rule = json.dumps(rule)
        super(Website, self).__init__()
    
    def get_rule(self):
        try:
            rule = json.loads(self.rule.replace('\'', '"'))
            return rule
        except Exception as exp:
            return {'error':str(exp)}

    def __repr__(self):
        return f'<Vedio : {self.name}>'

    def __str__(self):
        return f'<Vedio : {self.name}>'
            


