import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('token') || ''
    },
    mutations: {
        SET_USER_TOKEN(state, token){
            localStorage.setItem('token', JSON.stringify(token))
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
        },

        CLEAR_USER_TOKEN (state) {
            localStorage.removeItem('token')
            location.reload()
        },

    },
    actions: {
        LOGIN({commit}, data) {
            return axios.post('http://127.0.0.1:5000/api/login', data=data)
                .then(rsp => {
                    console.log(rsp.data)
                    const token = rsp.data.token
                    localStorage.setItem('token', token)
                    commit('SET_USER_TOKEN', token)
                    console.log(this.$store.token)
                })
        },
        LOGOUT({commit}) {
            commit('CLEAR_USER_TOKEN')
        }
    },
    modules: {
    }
})
